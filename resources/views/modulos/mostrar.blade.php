@extends('layouts.app')

@section('contenido')
    <div class=" accent-3 relative nav-sticky" style="background-color: #F4C1E1!important">
        <div class="container-fluid text-white" style="background-color: #F4C1E1">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        RESERVAS
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a href="{{ route('reservas') }}" class="nav-link"><i class="icon icon-home2"></i>Reservas</a>
                    </li>

                    <li>
                        <a href="{{ route('crearReserva') }}" class="nav-link"><i class="icon icon-user-plus"></i>Nueva
                            Reserva</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>



    <div class="container-fluid">
        <div class="row my-3">
            <div class="col-md-10  offset-md-1">
                <form action="{{route('storeReservaFianl')}}" method="post" id="Reserva">
                    @csrf
                    <div class="card no-b  no-r">
                        <div class="card-body">

                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Nombre</label>
                                        <input id="name" placeholder="Nombre" disabled value="{{$reserva->nombre}}" class="form-control r-0 light s-12 "
                                            name="nombre" type="text">
                                        <input name="idReserva" id="idReserva" type="hidden" value="{{ $reserva->id }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Fecha</label>
                                        <input id="fecha" disabled value="{{$reserva->fecha}}" name="fecha" placeholder="Fecha"
                                            class="form-control r-0 light s-12 " name="fecha" type="date">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Dependencia</label>
                                        <select name="dependencia" onchange="mostrarHorarios()"
                                            class="form-control r-0 light s-12" disabled id="dependencia">
                                            <option value="">SELECCIONE</option>
                                            @foreach ($dependencias as $dependencia)
                                                <option value="{{ $dependencia->id }}" @if($dependencia_re->idReservado == $dependencia->id) selected @endif>{{ $dependencia->dependencia }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Horario 1:</label>
                                        <select name="horario1" disabled onchange="mostrarRecursos()"
                                            class="form-control r-0 light s-12" id="horario1">
                                            <option value="">SELECCIONE</option>
                                            @foreach ($horarios as $horario)
                                                <option value="" @if($horario->id == $reserva->horario1->id) selected @endif>{{$horario->horario}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group m-0">
                                        <label for="name" disabled class="col-form-label s-12">Horario 2:</label>
                                        <select name="horario2" disabled class="form-control r-0 light s-12" id="horario2">
                                            <option value="">SELECCIONE</option>
                                            @foreach ($horarios as $horario)
                                                <option value="" @if($reserva->horario2 != null) @if($horario->id == $reserva->horario2->id) selected @endif @endif>{{$horario->horario}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                                <div class="row clearfix mt-5" id="html">
                                    {!!$recursos!!}
                                </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">

                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>
@endsection
