@extends('layouts.app')

@section('contenido')
    <div class=" accent-3 relative nav-sticky" style="background-color: #F4C1E1!important">
        <div class="container-fluid text-white" style="background-color: #F4C1E1">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        RESERVAS
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <a href="{{ route('reservas') }}" class="nav-link"><i class="icon icon-home2"></i>Reservas</a>
                    </li>

                    <li>
                        <a href="{{ route('crearReserva') }}" class="nav-link"><i class="icon icon-user-plus"></i>Nueva
                            Reserva</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>



    <div class="container-fluid">
        <div class="row my-3">
            <div class="col-md-10  offset-md-1">
                <form action="{{route('storeReservaFianl')}}" method="post" id="Reserva">
                    @csrf
                    <div class="card no-b  no-r">
                        <div class="card-body">
                            <h5 class="card-title">Crear Recurso</h5>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Nombre</label>
                                        <input id="name" placeholder="Nombre" class="form-control r-0 light s-12 "
                                            name="nombre" type="text">
                                        <input name="idReserva" id="idReserva" type="hidden" value="{{ $reserva->id }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Fecha</label>
                                        <input id="fecha" name="fecha" placeholder="Fecha"
                                            class="form-control r-0 light s-12 " name="fecha" type="date">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Dependencia</label>
                                        <select name="dependencia" onchange="mostrarHorarios()"
                                            class="form-control r-0 light s-12" id="dependencia">
                                            <option value="">SELECCIONE</option>
                                            @foreach ($dependencias as $dependencia)
                                                <option value="{{ $dependencia->id }}">{{ $dependencia->dependencia }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Horario 1:</label>
                                        <select name="horario1" onchange="mostrarRecursos()"
                                            class="form-control r-0 light s-12" id="horario1">
                                            <option value="">SELECCIONE</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Horario 2:</label>
                                        <select name="horario2" class="form-control r-0 light s-12" id="horario2">
                                            <option value="">SELECCIONE</option>
                                        </select>
                                    </div>
                                </div>
                            </div>



                                <div class="row clearfix" style="display: none" id="recursos">
                                    <div class="col-md-10">
                                        <label for="">Recurso</label>
                                        <select name="idProducto" id="idProducto" class="form-control x-100">
                                            <option value="0">-- SELECCIONE --</option>
                                            @foreach ($recursos as $recurso)
                                                <option value="{{$recurso->id}}">-- {{$recurso->recurso}} --</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="" class="text-white">.</label>
                                        <button type="button" onclick="agregar()"
                                            class=" w-100 btn btn-success">Agregar</button>
                                    </div>
                                </div>

                                <div class="row clearfix" id="html">
                                </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <button type="submit" class="btn btn-primary w-100"><i class="icon-save mr-2"></i>Guardar</button>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>
@endsection

@section('scriptss')
    <script>
        function mostrarHorarios() {
            const params = {
                fecha: $("#fecha").val()
            }
            axios.post('/api/getDisponibles', params).then((response) => {
                $("#horario1").html(response.data)
                $("#horario2").html(response.data)
            })
        }

        function mostrarRecursos() {
            $("#recursos").show()
        }

        function agregar(){
            const params = {
                idReserva: $("#idReserva").val(),
                tipo: 2,
                idReservado: $("#idProducto").val(),
            }

            axios.post('/api/agregarRecurso', params).then((response) => {
                $("#html").html(response.data)
            })
        }
    </script>
@endsection
