<div>
    <div class=" accent-3 relative nav-sticky" style="background-color: #F4C1E1!important">
        <div class="container-fluid text-white" style="background-color: #F4C1E1">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        RESERVAS
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <span class="nav-link" ><i class="icon icon-home2"></i>Reservas</span>
                    </li>

                    <li>
                        <a href="{{route('crearReserva')}}" class="nav-link"><i class="icon icon-user-plus"></i>Nueva
                            Reserva</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include("livewire.modulos.reservas.$view")
</div>
