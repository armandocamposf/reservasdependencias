<div class="container-fluid">
    <div class="row my-3">
        <div class="col-md-10  offset-md-1">
            <form action="#">
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <h5 class="card-title">Crear Recurso</h5>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Nombre</label>
                                    <input id="name" placeholder="Nombre" class="form-control r-0 light s-12 "
                                        wire:model='nombre' type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Fecha</label>
                                    <input id="fecha" placeholder="Fecha" class="form-control r-0 light s-12 "
                                        wire:model='fecha' type="date">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Dependencia</label>
                                    <select name="" v-model="dependencia"  wire:change="mostrarHorarios()" class="form-control r-0 light s-12" id="">
                                        <option value="">SELECCIONE</option>
                                        @foreach($dependencias as $dependencia)
                                        <option value="{{$dependencia->id}}">{{$dependencia->dependencia}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Horario 1:</label>
                                    <select name="" v-model="horario1" class="form-control r-0 light s-12" id="">
                                        <option value="">SELECCIONE</option>
                                        {{$horariosDisponibles}}
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Horario 2:</label>
                                    <select name="" v-model="horario2" class="form-control r-0 light s-12" id="">
                                        <option value="">SELECCIONE</option>
                                        {{$horariosDisponibles}}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <button type="button" wire:click='save()' class="btn btn-primary w-100"><i
                                class="icon-save mr-2"></i>Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
