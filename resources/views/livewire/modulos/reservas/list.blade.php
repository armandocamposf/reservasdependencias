<div class="row my-3 ml-3 mr-3">
    <div class="col-md-12">
        <div class="card r-0 shadow">
            <div class="table-responsive">
                <form>
                    <table class="table table-striped table-hover r-0">
                        <thead>
                            <tr class="no-b">
                                <th>Id</th>
                                <th>Fecha</th>
                                <th>Horarios</th>
                                <th>Nombre</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($reservas as $reserva)
                            <tr>
                                <td>{{ $reserva->id }}</td>
                                <td>{{ $reserva->fecha }}</td>
                                <td>{{ $reserva->horario1->horario }} @if($reserva->horario2 != null){{ $reserva->horario2->horario }}@endif</td>
                                <td>{{ $reserva->nombre }}</td>
                                <td>
                                    <a href="{{route('mostrar', $reserva->id)}}"><i class="s-24 icon-eye text-success" style="font-size: 30px"></i></a>
                                    <i class="s-24 icon-trash text-danger" wire:click='eliminar({{$reserva->id}})' style="font-size: 30px"></i>
                                </td>
                            </tr>
                            @empty
                            <tr class="text-center">
                                <td colspan="5" class="py-3 italic">No hay información</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>


</div>
