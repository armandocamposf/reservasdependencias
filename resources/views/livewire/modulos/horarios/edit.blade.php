<div class="container-fluid">
    <div class="row my-3">
        <div class="col-md-10  offset-md-1">
            <form action="#">
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <h5 class="card-title">Editar Horario</h5>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Inicio</label>
                                    <input id="name" placeholder="Horario" class="form-control r-0 light s-12 "
                                        wire:model='horario1' type="time">
                                </div>

                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Fin</label>
                                    <input id="name" placeholder="Horario" class="form-control r-0 light s-12 "
                                        wire:model='horario2' type="time">
                                </div>

                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <button type="button" wire:click='update()' class="btn btn-primary w-100"><i
                                class="icon-save mr-2"></i>Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
