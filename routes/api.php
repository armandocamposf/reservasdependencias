<?php

use App\Http\Controllers\RelacionReservaController;
use App\Http\Controllers\ReservaController;
use App\Models\RelacionReserva;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('getDisponibles', [ReservaController::class, 'mostrarHorarios']);
Route::post('agregarRecurso', [RelacionReservaController::class, 'store']);

