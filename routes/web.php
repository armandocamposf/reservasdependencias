<?php
namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('dependencias', [DependenciaController::class, 'index'])->name('dependencias');
Route::get('recursos', [RecursoController::class, 'index'])->name('recursos');
Route::get('horarios', [HorarioController::class, 'index'])->name('horarios');
Route::get('reservas', [ReservaController::class, 'index'])->name('reservas');
Route::get('create-reserva', [ReservaController::class, 'create'])->name('crearReserva');
Route::post('storeReservaFianl', [ReservaController::class, 'actualizarFinal'])->name('storeReservaFianl');
Route::get('mostrar/{idReserva}', [ReservaController::class, 'mostrar'])->name('mostrar');
