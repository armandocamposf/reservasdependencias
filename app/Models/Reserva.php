<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'idHorario1',
        'idHorario2',
        'fecha',
    ];

    public function horario1()
    {
        return $this->hasOne(Horario::class, 'id', 'idHorario1');
    }

    public function horario2()
    {
        return $this->hasOne(Horario::class, 'id', 'idHorario2');
    }

    public function relaciones()
    {
        return $this->hasMany(RelacionReserva::class, 'idReserva', 'id');
    }
}
