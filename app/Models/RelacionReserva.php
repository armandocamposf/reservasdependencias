<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelacionReserva extends Model
{
    use HasFactory;

    protected $filable = [
        'idReserva',
        'tipo',
        'idReservado',
    ];

    public function reservado()
    {
        return $this->hasOne(Recurso::class, 'id', 'idReservado');
    }

    public function dependencia()
    {
        return $this->hasOne(Dependencia::class, 'id', 'idReservado');
    }
}
