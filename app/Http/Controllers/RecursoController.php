<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRecursoRequest;
use App\Http\Requests\UpdateRecursoRequest;
use App\Models\Recurso;

class RecursoController extends Controller
{
    public function index()
    {
        return view('modulos.recursosModule');
    }

}
