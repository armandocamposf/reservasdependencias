<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreReservaRequest;
use App\Http\Requests\UpdateReservaRequest;
use App\Models\Dependencia;
use App\Models\Horario;
use App\Models\Recurso;
use App\Models\RelacionReserva;
use App\Models\Reserva;
use Illuminate\Http\Request;

class ReservaController extends Controller
{
    public function index()
    {
        return view('modulos.reservaModule');
    }

    public function create()
    {
        $reserva = Reserva::create([
            'nombre' => "varios",
            'idHorario1' => 1,
            'fecha' => date('Y-m-d')
        ]);

        $recursos = Recurso::all();
        $dependencias = Dependencia::all();
        return view('modulos.crearReserva', compact('dependencias', 'reserva', 'recursos'));
    }


    public function mostrarHorarios(Request $request)
    {
        $horarios = Horario::all();
        $reservas = Reserva::whereFecha($request->fecha)->get();
        $horariosDisponibles = "<option value=''>SELECCIONE</option>";
        foreach ($horarios as $horario) {
            $disabled = false;
            foreach ($reservas as $reserva) {
                if ($reserva->idHorario1 == $horario->id) {
                    $disabled = true;
                }
            }

            if ($disabled == true) {
                $horariosDisponibles .= "<option value='' disabled>$horario->horario</option>";
            } else {
                $horariosDisponibles .= "<option value='$horario->id'>$horario->horario</option>";
            }
        }

        return $horariosDisponibles;
    }

    public function actualizarFinal(Request $request)
    {
        $reserva = Reserva::find($request->idReserva);
        $reserva->nombre = $request->nombre;
        $reserva->fecha = $request->fecha;
        $reserva->idHorario1 = $request->horario1;
        $reserva->idHorario2 = $request->horario2;
        $reserva->save();

        $reserva = new RelacionReserva();
        $reserva->idReserva = $request->idReserva;
        $reserva->tipo = 1;
        $reserva->idReservado = $request->idProducto;
        $reserva->save();

        return redirect()->route('reservas');
    }

    public function mostrar($idReserva)
    {
        $reserva = Reserva::where('id', $idReserva)->with(['horario1', 'horario2', 'relaciones',])->first();

        $dependencia_re = RelacionReserva::where('tipo', 1)->where('idReserva', $reserva->id)->with('dependencia')->first();

        $recursos = Recurso::all();
        $dependencias = Dependencia::all();
        $horarios = Horario::all();


        $reservas_rel = RelacionReserva::where('idReserva', $reserva->id)->with('reservado')->get();

        $html = "<table class='table'>";
        $html .= "<tr>
            <th>Recurso</th>
        </tr>";

        foreach ($reservas_rel as $rel) {
            if($rel->reservado != null){
                $html .= "<tr><td>" . $rel->reservado->recurso . "</td></tr>";
            }

        }

        $html .= "</table>";

        $recursos = $html;


        return view('modulos.mostrar', compact('reserva', 'recursos', 'dependencias', 'dependencia_re', 'horarios', 'recursos'));

    }

}
