<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRelacionReservaRequest;
use App\Http\Requests\UpdateRelacionReservaRequest;
use App\Models\RelacionReserva;
use Illuminate\Http\Request;

class RelacionReservaController extends Controller
{
    public function store(Request $request)
    {
       $reserva = new RelacionReserva();
       $reserva->idReserva = $request->idReserva;
       $reserva->tipo = $request->tipo;
       $reserva->idReservado = $request->idReservado;
       $reserva->save();

       $reservas = RelacionReserva::where('idReserva', $request->idReserva)->with('reservado')->get();

       $html = "<table class='table'>";
        $html .= "<tr>
            <th>Recurso</th>
        </tr>";

        foreach ($reservas as $reserva ) {
            $html .= "<tr><td>".$reserva->reservado->recurso."</td></tr>";
        }

       $html .="</table>";

       return $html;

    }
}
