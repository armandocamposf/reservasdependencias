<?php

namespace App\Http\Livewire;

use App\Models\Dependencia;
use Livewire\Component;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class DependenciaComponent extends Component
{
    use LivewireAlert;


    public $view = "list";
    public $dependencia, $idEditando;

    public function render()
    {
        $dependencias = Dependencia::all();
        return view('livewire.dependencia-component', compact('dependencias'));
    }

    public function new()
    {
        $this->view = "create";
    }

    public function save()
    {
        $dep = Dependencia::create([
            'dependencia' => $this->dependencia
        ]);

        $this->dependencia = "";
        $this->view = "list";
        $this->alert('success', 'Dependencia creada con exito!');
    }

    public function edit($idDependencia)
    {
        $dep = Dependencia::find($idDependencia);
        $this->idEditando = $dep->id;
        $this->dependencia = $dep->dependencia;

        $this->view = "edit";
    }

    public function update()
    {
        $dep = Dependencia::find($this->idEditando);
        $dep->dependencia = $this->dependencia;
        $dep->save();

        $this->view = "list";
        $this->dependencia = "";
        $this->alert('success', 'Dependencia editada con exito!');
    }

    public function eliminar($idDependencia)
    {
        $dep = Dependencia::find($idDependencia)->delete();

        $this->view = "list";
        $this->alert('success', 'Dependencia eliminada con exito!');
    }

    public function user()
    {
        $this->view = "list";
    }
}
