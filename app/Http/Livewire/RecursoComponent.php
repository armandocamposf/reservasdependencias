<?php

namespace App\Http\Livewire;

use App\Models\Recurso;
use Livewire\Component;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class RecursoComponent extends Component
{
    use LivewireAlert;
    public $view = "list";
    public $recurso, $idEditando;
    public function render()
    {
        $recursos = Recurso::all();
        return view('livewire.recurso-component', compact('recursos'));
    }

    public function new()
    {
        $this->view = "create";
    }

    public function user()
    {
        $this->view = "list";
    }

    public function save()
    {
        $dep = Recurso::create([
            'recurso' => $this->recurso
        ]);

        $this->recurso = "";
        $this->view = "list";
        $this->alert('success', 'Recurso creado con exito!');
    }

    public function edit($idRecurso)
    {
        $dep = Recurso::find($idRecurso);
        $this->idEditando = $dep->id;
        $this->recurso = $dep->recurso;

        $this->view = "edit";
    }

    public function update()
    {
        $dep = Recurso::find($this->idEditando);
        $dep->recurso = $this->recurso;
        $dep->save();

        $this->view = "list";
        $this->recurso = "";
        $this->alert('success', 'Recurso editada con exito!');
    }

    public function eliminar($idDependencia)
    {
        $dep = Recurso::find($idDependencia)->delete();

        $this->view = "list";
        $this->alert('success', 'Recurso eliminada con exito!');
    }
}
