<?php

namespace App\Http\Livewire;

use App\Models\{Reserva, Dependencia, Horario};
use Livewire\Component;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class ReservaComponent extends Component
{
    use LivewireAlert;
    public $view = "list";

    public $nombre, $fecha, $recursos = [], $dependencia, $horario, $dependencias = [], $horarios = [], $horariosDisponibles, $horario1, $horario2, $mostrar;
    public function render()
    {
        $reservas = Reserva::where('nombre', '!=', 'varios')->with(['horario1', 'horario2'])->get();
        return view('livewire.reserva-component', compact('reservas'));
    }

    public function user()
    {
        $this->view = "list";
    }

    public function new()
    {
        $this->dependencias = Dependencia::all();
        $this->view = "create";
    }


}
