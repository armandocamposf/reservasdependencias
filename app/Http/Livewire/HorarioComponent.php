<?php

namespace App\Http\Livewire;

use App\Models\Horario;
use App\Models\Recurso;
use Livewire\Component;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class HorarioComponent extends Component
{
    use LivewireAlert;
    public $view = "list";
    public $horario1, $horario2, $idEditando;


    public function render()
    {
        $horarios = Horario::all();
        return view('livewire.horario-component', compact('horarios'));
    }

    public function new()
    {
        $this->view = "create";
    }

    public function save()
    {
        $horario = $this->horario1 . " - " . $this->horario2;
        $dep = Horario::create([
            'horario' => $horario
        ]);

        $this->horario1 = "";
        $this->horario2 = "";
        $this->recurso = "";
        $this->view = "list";
        $this->alert('success', 'Horario creado con exito!');
    }

    public function edit($idDependencia)
    {
        $dep = Horario::find($idDependencia);
        $this->idEditando = $dep->id;
        $horario = explode(' - ', $dep->horario);
        $this->horario1 = $horario[0];
        $this->horario2 = $horario[1];

        $this->view = "edit";
    }

    public function update()
    {
        $horario = $this->horario1 . " - " . $this->horario2;
        $dep = Horario::find($this->idEditando);
        $dep->horario = $horario;
        $dep->save();

        $this->view = "list";
        $this->horario = "";
        $this->alert('success', 'Horario editado con exito!');
    }

    public function eliminar($idDependencia)
    {
        $dep = Horario::find($idDependencia)->delete();

        $this->view = "list";
        $this->alert('success', 'Horario eliminado con exito!');
    }

    public function user()
    {
        $this->view = "list";
    }
}
